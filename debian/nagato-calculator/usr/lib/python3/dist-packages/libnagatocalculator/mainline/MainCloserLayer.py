
from libnagato4.maincloser.MainCloser import AbstractMainCloser
from libnagatocalculator.mainline.HeaderNotifyLayer import (
    NagatoHeaderNotifyLayer
    )

class NagatoMainCloserLayer(AbstractMainCloser):

    def _on_initialize(self):
        NagatoHeaderNotifyLayer(self)
