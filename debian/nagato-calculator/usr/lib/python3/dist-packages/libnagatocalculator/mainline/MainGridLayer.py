
from libnagato4.mainline.MainGridLayer import AbstractMainGridLayer
from libnagatocalculator.ui.TextView import NagatoTextView
from libnagatocalculator.ui.button.group.NumberKeys import NagatoNumberKeys
from libnagatocalculator.ui.button.group.FunctionKeys import NagatoFunctionKeys
from libnagatocalculator.ui.button.group.SpecialKeys import NagatoSpecialKeys
from libnagatocalculator.ui.KeyInput import NagatoKeyInput


class NagatoMainGridLayer(AbstractMainGridLayer):

    def _yuki_n_input(self, key):
        self._text_view.input(key)

    def _yuki_n_loopback_add_main_widget(self, parent):
        self._text_view = NagatoTextView(parent)
        NagatoKeyInput(parent)
        NagatoNumberKeys(parent)
        NagatoFunctionKeys(parent)
        NagatoSpecialKeys(parent)
        parent.set_column_homogeneous(True)
        # parent.set_vomogeneous(True)
