
from libnagato4.Object import NagatoObject

from gi.repository import Gdk


class NagatoKeyInput(NagatoObject):

    def _on_key_press(self, window, event):
        self._raise("YUKI.N > input", Gdk.keyval_name(event.keyval))

    def __init__(self, parent):
        self._parent = parent
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_window.connect("key-press-event", self._on_key_press)
