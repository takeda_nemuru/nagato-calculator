
from gi.repository import Gtk
from gi.repository import GtkSource
from libnagato.Object import NagatoObject
from libnagato.Ux import Unit
from libnagatocalculator.util.KeyvalTranslator import NagatoKeyvalTranslator
from libnagatocalculator.util.Executor import NagatoExecutor


class NagatoTextView(GtkSource.View, NagatoObject):

    def _inform_buffer(self):
        return self.get_buffer()

    def input(self, key):
        self._keyval_translator.try_translate(key)
        if key == "BackSpace":
            yuki_buffer = self.get_buffer()
            yuki_count = yuki_buffer.get_char_count()
            yuki_start_iter = yuki_buffer.get_iter_at_offset(yuki_count -1)
            yuki_end_iter = yuki_buffer.get_end_iter()
            yuki_buffer.delete(yuki_start_iter, yuki_end_iter)
        self._executor.try_execute(key)

    def __init__(self, parent):
        self._parent = parent
        Gtk.TextView.__init__(self)
        self.set_justification(Gtk.Justification.RIGHT)
        self.set_top_margin(Unit(2))
        self.set_bottom_margin(Unit(2))
        self.set_left_margin(Unit(2))
        self.set_right_margin(Unit(2))
        self.set_can_focus(False)
        self.set_editable(False)
        self._keyval_translator = NagatoKeyvalTranslator(self)
        self._executor = NagatoExecutor(self)
        self._raise("YUKI.N > attach to grid", (self, (0, -1, 9, 2)))
