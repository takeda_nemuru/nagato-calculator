
from gi.repository import Gtk
from libnagato.Object import NagatoObject


class NagatoFunction(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        self._raise("YUKI.N > input", self._keyval)

    def __init__(self, parent, visible, keyval):
        self._parent = parent
        Gtk.Button.__init__(self, label=visible)
        self._keyval = keyval
        self.set_can_focus(False)
        self.set_hexpand(True)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > css", (self, "numkey"))
