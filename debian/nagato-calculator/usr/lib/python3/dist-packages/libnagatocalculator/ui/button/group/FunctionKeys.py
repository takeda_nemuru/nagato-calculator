
from libnagato.Object import NagatoObject
from libnagatocalculator.ui.button.Function import NagatoFunction


class NagatoFunctionKeys(NagatoObject):

    def _attach(self, widget, position):
        yuki_data = widget, position
        self._raise("YUKI.N > attach to grid", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._attach(NagatoFunction(self, ".", "perid"), (0, 3, 1, 2))
        self._attach(NagatoFunction(self, "+", "plus"), (6, 3, 1, 1))
        self._attach(NagatoFunction(self, "-", "minus"), (6, 4, 1, 1))
        self._attach(NagatoFunction(self, "*", "asterisk"), (7, 3, 1, 1))
        self._attach(NagatoFunction(self, "/", "slash"), (7, 4, 1, 1))
        self._attach(NagatoFunction(self, "=", "equal"), (8, 3, 1, 2))
