
from libnagato.Object import NagatoObject

TARGETS = {
    "plus": "+",
    "minus": "-",
    "asterisk": "*",
    "slash": "/",
    "period": ".",
    "parenleft": "(",
    "parenright": ")"
    }


class NagatoKeyvalTranslator(NagatoObject):

    def _translated(self, char):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_end_iter = yuki_buffer.get_end_iter()
        yuki_buffer.insert(yuki_end_iter, char, -1)

    def try_translate(self, key):
        if key.isnumeric():
            self._translated(key)
        if key in TARGETS:
            self._translated(TARGETS[key])

    def __init__(self, parent):
        self._parent = parent
