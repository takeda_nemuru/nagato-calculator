
from libnagato.Object import NagatoObject


class NagatoExecutor(NagatoObject):

    def _execute(self):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_calc = yuki_buffer.props.text
        try:
            yuki_result = eval(yuki_calc)
            yuki_buffer.props.text = str(yuki_result)
        except (NameError, SyntaxError) as unaaa:
            yuki_buffer.props.text = "syntax error"

    def try_execute(self, key):
        if key in ["=", "equal", "Return"]:
            self._execute()

    def __init__(self, parent):
        self._parent = parent
