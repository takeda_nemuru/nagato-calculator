
from libnagato4.Object import NagatoObject
from libnagatocalculator.util.Executor import NagatoExecutor


class NagatoBackspace(NagatoObject):

    def _translated(self):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_count = yuki_buffer.get_char_count()
        yuki_start_iter = yuki_buffer.get_iter_at_offset(yuki_count -1)
        yuki_end_iter = yuki_buffer.get_end_iter()
        yuki_buffer.delete(yuki_start_iter, yuki_end_iter)

    def try_translate(self, key):
        if key == "BackSpace":
            self._translated()
        else:
            self._executor.try_execute(key)

    def __init__(self, parent):
        self._parent = parent
        self._executor = NagatoExecutor(self)
