
from libnagato4.Object import NagatoObject
from libnagatocalculator.key.bind.Charactor import NagatoCharactor


class NagatoNumeric(NagatoObject):

    def _translated(self, char):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_end_iter = yuki_buffer.get_end_iter()
        yuki_buffer.insert(yuki_end_iter, char, -1)

    def try_translate(self, key):
        if key.isnumeric():
            self._translated(key)
        else:
            self._charactor.try_translate(key)

    def __init__(self, parent):
        self._parent = parent
        self._charactor = NagatoCharactor(self)
