
from libnagato4.Object import NagatoObject
from libnagatocalculator.key.bind.Backspace import NagatoBackspace

TARGETS = {
    "plus": "+",
    "minus": "-",
    "asterisk": "*",
    "slash": "/",
    "period": ".",
    "parenleft": "(",
    "parenright": ")"
    }


class NagatoCharactor(NagatoObject):

    def _translated(self, char):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_end_iter = yuki_buffer.get_end_iter()
        yuki_buffer.insert(yuki_end_iter, char, -1)

    def try_translate(self, key):
        if key in TARGETS:
            self._translated(TARGETS[key])
        else:
            self._backspace.try_translate(key)

    def __init__(self, parent):
        self._parent = parent
        self._backspace = NagatoBackspace(self)
