
from libnagato4.Object import NagatoObject
from libnagatocalculator.ui.button.Number import NagatoNumber


class NagatoNumberKeys(NagatoObject):

    def _attach(self, widget, position):
        yuki_data = widget, position
        self._raise("YUKI.N > attach to grid", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._attach(NagatoNumber(self, "5"), (1, 3, 1, 1))
        self._attach(NagatoNumber(self, "6"), (2, 3, 1, 1))
        self._attach(NagatoNumber(self, "7"), (3, 3, 1, 1))
        self._attach(NagatoNumber(self, "8"), (4, 3, 1, 1))
        self._attach(NagatoNumber(self, "9"), (5, 3, 1, 1))
        self._attach(NagatoNumber(self, "0"), (1, 4, 1, 1))
        self._attach(NagatoNumber(self, "1"), (2, 4, 1, 1))
        self._attach(NagatoNumber(self, "2"), (3, 4, 1, 1))
        self._attach(NagatoNumber(self, "3"), (4, 4, 1, 1))
        self._attach(NagatoNumber(self, "4"), (5, 4, 1, 1))
