
from libnagato4.Object import NagatoObject
from libnagatocalculator.ui.button.Function import NagatoFunction


class NagatoSpecialKeys(NagatoObject):

    def _attach(self, widget, position):
        yuki_data = widget, position
        self._raise("YUKI.N > attach to grid", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        self._attach(NagatoFunction(self, "sqrt", ""), (2, 2, 1, 1))
        self._attach(NagatoFunction(self, "pow",""), (3, 2, 1, 1))
        self._attach(NagatoFunction(self, "mod", ""), (4, 2, 1, 1))
        self._attach(NagatoFunction(self, "div", ""), (5, 2, 1, 1))
        self._attach(NagatoFunction(self, "(", "parenleft"), (6, 2, 1, 1))
        self._attach(NagatoFunction(self, ")", "parenright"), (7, 2, 1, 1))
        self._attach(NagatoFunction(self, "clear", ""), (8, 1, 1, 1))
        self._attach(NagatoFunction(self, "del", "BackSpace"), (8, 2, 1, 1))
