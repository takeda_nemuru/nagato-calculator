
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoNumber(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        self._raise("YUKI.N > input", self._keyval)

    def __init__(self, parent, number):
        self._parent = parent
        Gtk.Button.__init__(self, label=number)
        self._keyval = number
        self.set_can_focus(False)
        self.set_hexpand(True)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > css", (self, "numkey"))
