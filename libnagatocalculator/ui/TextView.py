
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatocalculator.ui.TextViewAttributes import HaruhiTextViewAttributes
from libnagatocalculator.key.bind.Numeric import NagatoNumeric


class NagatoTextView(Gtk.TextView, NagatoObject):

    def _inform_buffer(self):
        return self.get_buffer()

    def input(self, key):
        self._numeric.try_translate(key)

    def __init__(self, parent):
        self._parent = parent
        Gtk.TextView.__init__(self)
        HaruhiTextViewAttributes(self)
        self._numeric = NagatoNumeric(self)
        self._raise("YUKI.N > attach to grid", (self, (0, -1, 9, 2)))
