
from gi.repository import Gtk
from libnagato4.Ux import Unit


class HaruhiTextViewAttributes:

    def __init__(self, text_view):
        text_view.set_justification(Gtk.Justification.RIGHT)
        text_view.set_top_margin(Unit(2))
        text_view.set_bottom_margin(Unit(2))
        text_view.set_left_margin(Unit(2))
        text_view.set_right_margin(Unit(2))
        text_view.set_can_focus(False)
        text_view.set_editable(False)
